package xyz.linye.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import xyz.linye.domain.User;

@Configuration
@ComponentScan(value = "xyz.linye")
public class MyConfig {

    @Bean(name = "user")
    public User getUser(){
        return new User("111","111");
    }
}
